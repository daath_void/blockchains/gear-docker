# gear-docker

## OPÇÃO 1 - Processo com os scripts

Update S.O.


```
apt update
```


```
apt install -y git
```


```
cd /opt
```


```
git clone https://gitlab.com/daath_void/blockchains/gear-docker.git
```


```
cd /opt/gear-docker
```

```
./install-gear-node.sh
```

```
nano .env
```


```
update-gear-node.sh
```



##  OPÇÃO 2 - Procesos manual

Build:

```
docker build -t gear-node . 
```

Run:

``` 
docker run --name gear-node -p 30333:30333 -p 9615:9615 -p 9933:9933 -p 9944:9944 -e "NODE_NAME=INSERT_NODE_NAME_HERE" gear-node 
```

Build and Run with Compose:

Edit docker-compose.yml and change NODE_NAME value

Create local dir:

```
sudo mkdir -p /srv/gear/.local

```

Change permissions:

```
chown 1000 /srv/gear/.local
```


Exec:

``` 
docker-compose up -d 
```

Telemetry:

https://telemetry.gear-tech.io./
