#!/bin/bash

apt remove docker docker-engine docker.io containerd runc

apt update

apt install -y ca-certificates curl gnupg lsb-release git

mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update

apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

ln -s /usr/libexec/docker/cli-plugins/docker-compose /usr/bin/docker-compose

mkdir -p /srv/gear/.local

chown 1000 /srv/gear/.local

cd /opt

git clone https://gitlab.com/daath_void/blockchains/gear-docker.git

cd /opt/gear-docker

/usr/bin/docker-compose down
/usr/bin/docker build --force-rm --pull --no-cache -t gear-node .
/usr/bin/docker-compose up -d

cp update-gear-node.sh /usr/local/bin

chmod +x /usr/local/bin/update-gear-node.sh

/usr/bin/docker ps 

sleep 5

/usr/bin/docker logs --since 1m gear-node
