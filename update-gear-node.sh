#!/bin/bash

cd /opt/gear-docker

/usr/bin/docker-compose down
docker rmi $(docker images -a -q)
/usr/bin/docker build --force-rm --pull --no-cache -t gear-node .
/usr/bin/docker-compose up -d


/usr/bin/docker ps 

sleep 5

/usr/bin/docker logs --since 1m gear-node
