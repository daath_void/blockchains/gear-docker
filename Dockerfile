FROM debian:latest

WORKDIR /tmp

ARG USERNAME=gear
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && apt update \
    && apt install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN apt update && apt install -y wget tar xz-utils

RUN wget https://get.gear.rs/gear-nightly-linux-x86_64.tar.xz && \
    tar xvf gear-nightly-linux-x86_64.tar.xz && \
    rm gear-nightly-linux-x86_64.tar.xz &&\
    mv gear /usr/local/bin

EXPOSE 30333/tcp
EXPOSE 9615/tcp
EXPOSE 9933/tcp
EXPOSE 9944/tcp

WORKDIR /home/$USERNAME

USER $USERNAME

CMD  /usr/local/bin/gear --name "$NODE_NAME" --telemetry-url "ws://telemetry-backend-shard.gear-tech.io:32001/submit 0"
